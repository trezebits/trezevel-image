<?php

namespace Modules\Image\Entities;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    // protected $fillable = [];

    public static $rules = [
        'image' => 'mimes:jpeg,bmp,png,jpg'
    ];

    public static $messages = [
        'image.mimes' => 'A imagem não possui um formato correto'
    ];
}
