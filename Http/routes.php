<?php

Route::group(['middleware' => 'web', 'prefix' => 'image', 'namespace' => 'Modules\Image\Http\Controllers'], function()
{
    Route::get('/', 'ImageController@index');
});
